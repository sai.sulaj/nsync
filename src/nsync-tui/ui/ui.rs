use std::{io, path};
use termion::event;
use tui::{self, backend, layout};
use snafu::ResultExt;

use super::{note_list, input_field};
use crate::{types, events};
use nsync;

pub type Backend = backend::TermionBackend<termion::raw::RawTerminal<io::Stdout>>;
type Terminal = tui::Terminal<Backend>;

pub enum Mode {
    Normal,
    AddingNote,
}

pub struct Ui {
    note_list: note_list::NoteList,
    input_field: input_field::InputField,
    events: events::Events,
    mode: Mode,
    nsync: nsync::Nsync,
}
impl Ui {
    pub fn new(path: path::PathBuf) -> types::Result<Self> {
        Ok(Ui {
            note_list: note_list::NoteList::new(),
            input_field: input_field::InputField::new(),
            events: events::Events::with_config(events::Config::default()),
            mode: Mode::Normal,
            nsync: nsync::Nsync::new(path).context(types::NsyncError)?,
        })
    }

    fn handle_draw(&mut self, frame: &mut tui::Frame<Backend>) {
        let vertical_constraints = match self.mode {
            Mode::Normal => vec![
                layout::Constraint::Percentage(100),
            ],
            Mode::AddingNote => vec![
                 layout::Constraint::Percentage(95),
                 layout::Constraint::Percentage(5),
            ],
        };

        let vertical_chunks = layout::Layout::default()
            .direction(layout::Direction::Vertical)
            .margin(1)
            .constraints(vertical_constraints.as_ref())
            .split(frame.size());

        let horizontal_chunks = layout::Layout::default()
            .direction(layout::Direction::Horizontal)
            .margin(5)
            .constraints(
                [
                    layout::Constraint::Percentage(30),
                    layout::Constraint::Percentage(70),
                ]
                .as_ref(),
            )
            .split(vertical_chunks[0]);

        if let Mode::AddingNote = self.mode {
            self.input_field.draw(frame, vertical_chunks[1]);
        }

        self.note_list.draw(frame, horizontal_chunks[0]);
    }

    fn handle_input(&mut self, key: event::Key) {
        match key {
            event::Key::Char('n') => self.to_adding_note(),
            event::Key::Backspace => self.note_list.delete_selected(),
            _ => self.note_list.on_key(key)
        };
    }

    fn to_adding_note(&mut self) {
        self.mode = Mode::AddingNote;
        self.events.disable_exit_key();
    }

    fn to_normal(&mut self) {
        self.mode = Mode::Normal;
        self.events.enable_exit_key();
    }

    pub fn start(&mut self, terminal: &mut Terminal) -> types::Result<()> {
        terminal.clear().context(types::DrawError)?;

        loop {
            terminal.draw(|f| self.handle_draw(f)).context(types::DrawError)?;

            match self.events.next().unwrap() {
                events::Event::Input(c) => {
                    match self.mode {
                        Mode::Normal => self.handle_input(c),
                        Mode::AddingNote => {
                            let text_field_event = self.input_field.handle_input(c);
                            if let Some(text_field_event) = text_field_event {
                                match text_field_event {
                                    input_field::TextFieldEvent::Exit => self.to_normal(),
                                    input_field::TextFieldEvent::Submit(new_note_title) => {
                                        self.note_list.add_note(nsync::notetree::Note::new(&new_note_title, ""));
                                        self.to_normal();
                                    },
                                };
                            }
                        },
                    };
                },
            }
        }
    }
}
