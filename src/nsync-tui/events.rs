use std::{sync, io, thread};
use std::sync::atomic;
use termion::event;
use termion::input::TermRead;
use crate::types;
use snafu::ResultExt;

pub enum Event<A> {
    Input(A),
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub exit_key: event::Key,
}
impl Default for Config {
    fn default() -> Self {
        Config {
            exit_key: event::Key::Char('q'),
        }
    }
}

pub struct Events {
    rx: sync::mpsc::Receiver<Event<event::Key>>,
    input_handle: thread::JoinHandle<()>,
    ignore_exit_key: sync::Arc<atomic::AtomicBool>,
}
impl Events {
    pub fn with_config(config: Config) -> Self {
        let (tx, rx) = sync::mpsc::channel();
        let ignore_exit_key = sync::Arc::new(atomic::AtomicBool::new(false));
        let input_handle = {
            let tx = tx.clone();
            let ignore_exit_key = ignore_exit_key.clone();

            thread::spawn(move || {
                let stdin = io::stdin();
                for evt in stdin.keys() {
                    if let Ok(key) = evt {
                        if let Err(err) = tx.send(Event::Input(key)) {
                            eprintln!("{}", err);
                            return;
                        }

                        if !ignore_exit_key.load(atomic::Ordering::Relaxed) && key == config.exit_key {
                            return;
                        }
                    }
                }
            })
        };

        Events {
            rx,
            input_handle,
            ignore_exit_key,
        }
    }

    pub fn next(&self) -> types::Result<Event<event::Key>> {
        self.rx.recv().context(types::UserInputError)
    }

    pub fn disable_exit_key(&mut self) {
        self.ignore_exit_key.store(true, atomic::Ordering::Relaxed);
    }

    pub fn enable_exit_key(&mut self) {
        self.ignore_exit_key.store(false, atomic::Ordering::Relaxed);
    }
}
