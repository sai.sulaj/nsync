use std::{io, path};
use termion::raw::IntoRawMode;
use tui::backend;
use dirs;

mod events;
mod types;
mod ui;

use clap;

fn read_cli<'a>() -> clap::ArgMatches<'a> {
    clap::App::new("Nsync")
        .version("0.1")
        .author("Saimir Sulaj <hola@saimirsulaj.com>")
        .about("Notetaking")
        .arg(clap::Arg::with_name("database")
             .short("db")
             .long("database")
             .value_name("DATABASE")
             .help("The path to the database directory. Defaults to $HOME/.nsync")
             .takes_value(true))
        .get_matches()
}

fn main() {
    let matches = read_cli();

    let path = match matches.value_of("database") {
        Some(path) => {
            let path = path::Path::new(path).to_path_buf();

            if !path.is_dir() {
                panic!("Path is not a directory.");
            }
            if !path.exists() {
                panic!("Directory does not exist.");
            }

            path
        },
        None => {
            let mut home_dir = dirs::home_dir().expect("Unable to find home directory");
            home_dir.push(".nsync");
            home_dir
        },
    };

    let stdout = io::stdout()
        .into_raw_mode()
        .expect("Unable to convert to raw mode");
    let backend = backend::TermionBackend::new(stdout);
    let mut terminal = tui::Terminal::new(backend)
        .unwrap();

    let mut ui = ui::Ui::new(path).unwrap();
    ui.start(&mut terminal).unwrap();
}
