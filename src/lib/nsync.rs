use git2;
use std::{path, fs};
use super::types;
use snafu::ResultExt;
use crate::notetree;

fn create_or_open_repo(path: &path::PathBuf) -> types::Result<git2::Repository> {
    let mut dot_git = path.clone();
    dot_git.push(".git");

    if dot_git.exists() {
        git2::Repository::open(path.as_path()).context(types::Init)
    } else {
        git2::Repository::init(path.as_path()).context(types::Init)
    }
}

pub struct Nsync {
    repo: git2::Repository,
    pub file_tree: notetree::NoteTree,
}
impl Nsync {
    pub fn new(path: path::PathBuf) -> types::Result<Self> {
        let database_path = path.join("database");

        if !database_path.exists() {
            fs::create_dir(&database_path).unwrap(); // TODO.
        }

        Ok(Nsync {
            repo: create_or_open_repo(&path)?,
            file_tree: notetree::NoteTree::build_note_tree(&database_path)?,
        })
    }
}
