use tui::{layout, backend, widgets, text, style};
use termion::event;

pub enum TextFieldEvent {
    Exit,
    Submit(String),
}

pub struct InputField {
    text: String,
    cursor_pos: u16,
}
impl InputField {
    pub fn new() -> Self {
        InputField {
            text: String::new(),
            cursor_pos: 0u16,
        }
    }

    fn cursor_left(&mut self) {
        if self.cursor_pos == 0 { return; }
        self.cursor_pos -= 1;
    }

    fn cursor_right(&mut self) {
        if self.cursor_pos as usize == self.text.len() { return; }
        self.cursor_pos += 1;
    }

    fn clear(&mut self) {
        self.text.clear();
        self.cursor_pos = 0;
    }

    pub fn handle_input(&mut self, key: event::Key) -> Option<TextFieldEvent> {
        match key {
            event::Key::Char('\n') => {
                let submit_text = self.text.clone();
                self.clear();
                return Some(TextFieldEvent::Submit(submit_text));
            },
            event::Key::Esc => {
                self.clear();
                return Some(TextFieldEvent::Exit);
            },
            event::Key::Char(c) => {
                self.text.push(c);
                self.cursor_right();
            },
            event::Key::Backspace => {
                if self.cursor_pos == 0 { return None; }
                self.text.remove(self.cursor_pos as usize - 1);
                self.cursor_left();
            },
            event::Key::Left => self.cursor_left(),
            event::Key::Right => self.cursor_right(),
            _ => (),
        };

        return None;
    }

    pub fn draw<B: backend::Backend>(&mut self, frame: &mut tui::Frame<B>, area: layout::Rect) {
        let text = text::Text::styled(self.text.clone(), style::Style::default().add_modifier(style::Modifier::RAPID_BLINK));
        let field = widgets::Paragraph::new(text)
            .block(
                widgets::Block::default()
                    .title("Note Title")
                    .borders(widgets::Borders::ALL)
            );
        frame.render_widget(field, area);
        frame.set_cursor(self.cursor_pos + area.x + 1, area.y + 1);
    }
}
