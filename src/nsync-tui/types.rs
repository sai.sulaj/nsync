use std::{io, sync::mpsc};
use snafu;

#[derive(Debug, snafu::Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Could not init database: {}", message))]
    InitDbError { message: String },
    #[snafu(display("Underlying Nsync error: {}", source))]
    NsyncError { source: nsync::types::Error },
    #[snafu(display("Draw error: {}", source))]
    DrawError { source: io::Error },
    #[snafu(display("Draw error: {}", source))]
    UserInputError { source: mpsc::RecvError },
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
