use std::{path, fs, cmp, io::prelude::*};
use crate::types;

#[derive(Debug)]
pub struct Note {
    pub title: String,
    pub snippet: String,
}
impl Note {
    pub fn new(title: &str, snippet: &str) -> Self {
        Note { title: title.to_string(), snippet: snippet.to_string() }
    }

    pub fn from_path(file_name: &str, path: &path::PathBuf) -> types::Result<Self> {
        let mut snippet = String::new();
        fs::File::open(&path)
            .unwrap() // TODO
            .take(200)
            .read_to_string(&mut snippet)
            .unwrap(); // TODO

        Ok(Note { title: file_name.to_string(), snippet })
    }
}

#[derive(Debug)]
pub enum Node {
    NoteNode(Note),
    DirNode(String, Vec<Node>),
}

fn sort_nodes_fn(a: &Node, b: &Node) -> cmp::Ordering {
    if let Node::DirNode(_, _) = a {
        if let Node::NoteNode(_) = b {
            return cmp::Ordering::Less;
        }
    }
    if let Node::DirNode(_, _) = b {
        if let Node::NoteNode(_) = a {
            return cmp::Ordering::Greater;
        }
    }

    let a_str = match a {
        Node::NoteNode(note) => note.title.to_string(),
        Node::DirNode(name, _) => name.to_string(),
    };
    let b_str = match b {
        Node::NoteNode(note) => note.title.to_string(),
        Node::DirNode(name, _) => name.to_string(),
    };
    a_str.to_lowercase().cmp(&b_str.to_lowercase())
}

fn build_note_tree(entry: fs::DirEntry) -> types::Result<(Node, usize)> {
    let metadata = entry.metadata().unwrap(); // TODO
    let name = entry.file_name().to_string_lossy().to_string();

    let next_node = if metadata.is_dir() {
        let mut nodes: (Vec<Node>, usize) = fs::read_dir(entry.path())
            .unwrap() // TODO
            .map(|entry| {
                let entry = entry.unwrap(); // TODO
                build_note_tree(entry).unwrap() // TODO
            })
            .fold((Vec::new(), 0), |mut acc, (node, child_count)| {
                acc.0.push(node);
                acc.1 += child_count;
                acc
            });

        nodes.0.sort_by(sort_nodes_fn);
        (Node::DirNode(name, nodes.0), nodes.1)
    } else {
        (Node::NoteNode(Note::from_path(&name, &entry.path())?), 1)
    };

    Ok(next_node)
}

pub struct NoteTree {
    tree: Node,
    file_count: usize,
}
impl NoteTree {
    pub fn build_note_tree(path: &path::PathBuf) -> types::Result<Self> {
        let root_name = path.file_name()
            .unwrap()
            .to_string_lossy()
            .to_string();

        let (dir_nodes, file_count): (Vec<Node>, usize) = fs::read_dir(path)
            .unwrap() // TODO
            .map(|entry| {
                let entry = entry.unwrap(); // TODO
                build_note_tree(entry).unwrap() // TODO
            })
            .fold((Vec::new(), 0), |mut acc, (node, child_count)| {
                acc.0.push(node);
                acc.1 += child_count;
                acc
            });

        Ok(NoteTree {
            tree: Node::DirNode(root_name, dir_nodes),
            file_count,
        })
    }

    pub fn len(&self) -> usize {
        self.file_count
    }

    pub fn get_node(&self, path: &[usize]) -> Option<&Node> {
        let mut current = &self.tree;

        for index in path.iter() {
            if let Node::DirNode(_, children) = current {
                let child = children.get(*index);

                if child.is_none() {
                    return None;
                }

                current = child.unwrap();
            }

            return None;
        }

        Some(current)
    }

    pub fn get_node_children_len(&self, path: &[usize]) -> Option<usize> {
        let mut current = &self.tree;

        for index in path.iter() {
            if let Node::DirNode(_, children) = current {
                let child = children.get(*index);

                if child.is_none() {
                    return None;
                }

                current = child.unwrap();
            }

            return None;
        }

        if let Node::DirNode(_, children) = current {
            return Some(children.len());
        }

        None
    }

    pub fn add_note(&mut self, path: &[usize], note: Note) {
        let mut current: &mut Node = &mut self.tree;

        for index in path.iter() {
            if let Node::DirNode(_, children) = current {
                let child = children.get_mut(*index);

                if child.is_none() {
                    return;
                }

                current = child.unwrap();
            }

            return;
        }

        if let Node::DirNode(_, children) = current {
            children.push(Node::NoteNode(note));
            children.sort_by(sort_nodes_fn);
        }
    }

    pub fn delete(&mut self, path: &[usize]) {
        if path.len() == 0 { return; }

        let mut current: &mut Node = &mut self.tree;

        let (root_path, target_index) = path.split_at(path.len() - 1);
        let target_index = target_index[0];

        for index in root_path.iter() {
            if let Node::DirNode(_, children) = current {
                let child = children.get_mut(*index);

                if child.is_none() {
                    return;
                }

                current = child.unwrap();
            }

            return;
        }

        if let Node::DirNode(_, children) = current {
            children.remove(target_index);
        }
    }
}
