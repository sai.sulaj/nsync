use git2;
use snafu;

#[derive(Debug, snafu::Snafu)]
#[snafu(visibility = "pub")]
pub enum Error {
    #[snafu(display("Could not init repo -> {}", source))]
    Init {
        source: git2::Error,
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
