use nsync::notetree;
use tui::{widgets, backend, layout, style};
use termion::event;

pub struct NoteList<'a> {
    notes: &'a notetree::NoteTree,
    ui_state: widgets::ListState,
    focused_indices: Vec<usize>,
}
impl<'a> NoteList<'a> {
    pub fn new(notetree: &'a notetree::NoteTree) -> Self {
        NoteList {
            notes: notetree,
            ui_state: widgets::ListState::default(),
            focused_indices: vec![],
        }
    }

    pub fn next(&mut self) {
        if self.focused_indices.len() == 0 {
            self.focused_indices.push(0);
            return;
        }

        let focused_root_children_len = self.notes
            .get_node_children_len(&self.focused_indices[..(self.focused_indices.len() - 1)]);
        if focused_root_children_len.is_none() {
            return;
        }
        let focused_root_children_len = focused_root_children_len.unwrap();

        let prev_index = *self.focused_indices.last().unwrap();
        let next_index = if prev_index >= focused_root_children_len - 1 {
            0
        } else {
            prev_index + 1
        };

        *self.focused_indices.last_mut().unwrap() = next_index;
    }

    pub fn prev(&mut self) {
        if self.focused_indices.len() == 0 {
            self.focused_indices.push(0);
            return;
        }

        let focused_root_children_len = self.notes
            .get_node_children_len(&self.focused_indices[..(self.focused_indices.len() - 1)]);
        if focused_root_children_len.is_none() {
            return;
        }
        let focused_root_children_len = focused_root_children_len.unwrap();

        let prev_index = *self.focused_indices.last().unwrap();
        let next_index = if prev_index == 0 {
            focused_root_children_len - 1
        } else {
            prev_index - 1
        };

        *self.focused_indices.last_mut().unwrap() = next_index;
    }

    pub fn on_key(&mut self, key: event::Key) {
        match key {
            event::Key::Char('k') => self.prev(),
            event::Key::Char('j') => self.next(),
            _ => (),
        };
    }

    pub fn add_note(&mut self, path: &[usize], note: notetree::Note) {
        self.notes.add_note(path, note);
    }

    pub fn delete_selected(&mut self) {
        self.notes.delete(&self.focused_indices);
    }

    pub fn draw<B: backend::Backend>(&mut self, frame: &mut tui::Frame<B>, area: layout::Rect) {
        unimplemented!();
        let note_items: Vec<widgets::ListItem> = self.notes.iter()
            .map(|note| widgets::ListItem::new(note.title.clone()))
            .collect();
        let block = widgets::Block::default()
            .title("Notes")
            .borders(widgets::Borders::ALL);
        let list_widget = widgets::List::new(note_items)
            .block(block)
            .highlight_style(
                style::Style::default()
                    .fg(style::Color::Black)
                    .bg(style::Color::Cyan)
            );
        frame.render_stateful_widget(list_widget, area, &mut self.ui_state);
    }
}
